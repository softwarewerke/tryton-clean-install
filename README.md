# tryton-clean-install

Usually I'm trying to keep server clean. For a clean install of tryton I'm using this procedure.

1. Debian minimal

Install a bare Debian system with ssh.

2. Install dependencies

Look at https://bitbucket.org/softwarewerke/tryton-clean-install/src/dependencies-3.8?at=master 
to install needed packages

3. Download sources

cd /opt
hg hg clone https://hg.tryton.org/trytond/ -r 3.8

4. Add config

Look at 


